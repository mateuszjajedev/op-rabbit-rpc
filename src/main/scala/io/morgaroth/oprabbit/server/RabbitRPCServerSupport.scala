package io.morgaroth.oprabbit.server

import cats.data.EitherT
import cats.implicits._
import com.spingo.op_rabbit._
import com.spingo.op_rabbit.properties.CorrelationId
import com.typesafe.scalalogging.LazyLogging
import io.morgaroth.oprabbit.{RPCError, RabbitControlActor}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

case class RpcData(replyTo: String, corrId: String)

case class RpcFailure(error: RPCError)

trait RabbitRPCServerSupport extends LazyLogging {

  def sendRpc[T](data: T, rpcInfo: RpcData)(implicit rc: RabbitControlActor, rm: RabbitMarshaller[T]) = {
    rc.actor ! Message.queue(data, rpcInfo.replyTo, Seq(CorrelationId(rpcInfo.corrId)))
  }

  def replyRpc[T](data: T)(implicit rc: RabbitControlActor, rm: RabbitMarshaller[T], rpcInfoMaybe: Option[RpcData]): Option[T] =
    rpcInfoMaybe.map { rpcInfo =>
      logger.info(s"completing rpc request with $data and rpc info = $rpcInfo")
      sendRpc(data, rpcInfo)
      data
    }

  def replyRpc[T](dataF: EitherT[Future, RPCError, T])(implicit rc: RabbitControlActor,
                                                       rm: RabbitMarshaller[T],
                                                       rme: RabbitMarshaller[RpcFailure],
                                                       rpcInfoMaybe: Option[RpcData]
  ): EitherT[Future, RPCError, Option[Either[RpcFailure, T]]] = {

    val value: EitherT[Future, RPCError, Option[Either[RpcFailure, T]]] = dataF.map(data => replyRpc(data).map[Either[RpcFailure, T]](Right(_)))
    value.recover {
      case error => replyRpc(RpcFailure(error)).map(Left(_))
    }
  }
}