package io.morgaroth.oprabbit

trait RPCError {
  def code: Int

  def description: String
}

case class UnrecognizedException(
                                  description: String,
                                  throwableClass: String,
                                  throwableMessage: String,
                                  cause: Either[String, UnrecognizedException],
                                  stackTrace: Vector[String],
                                ) extends RPCError {
  override val code = ErrorCodes.unrecognizedException
}

object ErrorCodes {
  val unrecognizedException = 1000
}

object UnrecognizedException {
  def from(t: Throwable, description: Option[String] = None): UnrecognizedException = {
    new UnrecognizedException(
      description.getOrElse(t.getMessage),
      t.getClass.getCanonicalName,
      t.getMessage,
      Option(t.getCause).map(UnrecognizedException.from(_)).map(Right(_)).getOrElse(Left("No Cause")),
      t.getStackTrace.toVector.map(_.toString)
    )
  }
}