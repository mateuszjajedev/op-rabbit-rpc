package io.morgaroth.oprabbit.config

import com.spingo.op_rabbit.Exchange
import com.spingo.op_rabbit.Exchange.Fanout
import com.spingo.op_rabbit.properties.Header
import com.spingo.op_rabbit.properties.HeaderValue.StringHeaderValue
import com.typesafe.config.Config
import com.typesafe.config.ConfigException.Missing

import scala.collection.JavaConverters._
import scala.language.implicitConversions
import scala.util.Try

object QueueConfig {
  def parse(config: Config): QueueConfig = {
    val exchangeName = config.getString("exchange-name")
    val messageTopics = Try(config.getStringList("message-topics").asScala.toList).getOrElse(List.empty)
    val queueName = config.getString("name")
    val qos = Try(config.getInt("qos")).getOrElse(3)
    val alternateExchange = Try(config.getString("alternate-exchange")).toOption
    val durable = Try(config.getBoolean("durable")).recover { case e: Missing => true }.get
    val autoDelete = Try(config.getBoolean("auto-delete")).recover { case e: Missing => false }.get
    QueueConfig(queueName, exchangeName, qos, messageTopics, alternateExchange, durable, autoDelete)
  }

  implicit def unwrapToFanout(obj: QueueConfig): Exchange[Fanout.type] = obj.getExchangeFanout
}

case class QueueConfig(
                        queueName: String,
                        exchangeName: String,
                        qos: Int,
                        routingList: List[String] = List.empty,
                        alternateExchange: Option[String] = None,
                        durable: Boolean = true,
                        autoDelete: Boolean = false
                      ) {

  private def getAlternateExchangeHeader = {
    alternateExchange.filter(_.length > 0).map { ae =>
      Header("alternate-exchange", StringHeaderValue(ae))
    }.toSeq
  }

  def getExchangeTopic: Exchange[Exchange.Topic.type] = {
    Exchange.topic(exchangeName, arguments = getAlternateExchangeHeader)
  }

  def getExchangeDirect: Exchange[Exchange.Direct.type] = {
    Exchange.direct(exchangeName, arguments = getAlternateExchangeHeader)
  }

  def getExchangeFanout: Exchange[Fanout.type] = Exchange.fanout(exchangeName)

}