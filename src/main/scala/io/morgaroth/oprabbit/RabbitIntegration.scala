package io.morgaroth.oprabbit

import com.spingo.op_rabbit._
import io.morgaroth.oprabbit.config.{ExchangeConfig, QueueConfig}
import org.json4s.Formats
import Json4sSupport.native._

import scala.concurrent.ExecutionContext

trait BaseRabbit {
  implicit def rc: RabbitControlActor

  implicit val rs: RecoveryStrategy = RecoveryStrategy.limitedRedeliver()

  implicit def formats: Formats
}

trait RabbitConsumer {

  protected def subscribe(queueConfig: QueueConfig)(handler: Handler)(implicit rabbitControl: RabbitControlActor, ex: ExecutionContext, rs: RecoveryStrategy): SubscriptionRef = {
    import Directives._
    Subscription.run(rabbitControl) {
      channel(qos = queueConfig.qos) {
        consume(Binding.topic(queue(queueConfig.queueName), queueConfig.routingList, queueConfig.getExchangeTopic))(handler)
      }
    }
  }
}

trait RabbitPublisher {

  protected def sendToQueue[T](message: T, to: Option[String], exchange: ExchangeConfig)(implicit rc: RabbitControlActor, m: RabbitMarshaller[T]) = {
    val routingKey: String = to.map(concreteTo => s"${exchange.routingKey}.$concreteTo").getOrElse(exchange.routingKey)
    rc.actor ! Message.topic(message, routingKey, exchange.exchangeName)
  }

  protected def publish[T](message: T, exchange: ExchangeConfig)(implicit rc: RabbitControlActor, m: RabbitMarshaller[T]) = sendToQueue(message, None, exchange)

  protected def send[T](message: T, to: String, exchange: ExchangeConfig)(implicit rc: RabbitControlActor, m: RabbitMarshaller[T]) = sendToQueue(message, Some(to), exchange)
}

trait RabbitPublisherIntegration extends RabbitPublisher with BaseRabbit {
  def publishExchange: ExchangeConfig

  def publish[T <: AnyRef](message: T) = sendToQueue(message, None, publishExchange)

  def send[T <: AnyRef](message: T, to: String) = sendToQueue(message, Some(to), publishExchange)
}

trait RabbitConsumerIntegration extends RabbitConsumer with BaseRabbit {
  def subscribeQueue: QueueConfig

  implicit def ex: ExecutionContext

  def subscribe(handler: Handler): SubscriptionRef = super.subscribe(subscribeQueue)(handler)
}

trait RabbitIntegration extends RabbitPublisherIntegration with RabbitConsumerIntegration

case class RabbitIntegrationImpl(publishExchange: ExchangeConfig, subscribeQueue: QueueConfig)(implicit val rc: RabbitControlActor, val ex: ExecutionContext, val formats: Formats) extends RabbitIntegration

case class RabbitPublishIntegrationImpl(publishExchange: ExchangeConfig)(implicit val rc: RabbitControlActor, val formats: Formats) extends RabbitPublisherIntegration

case class RabbitSubscriberIntegrationImpl(subscribeQueue: QueueConfig)(implicit val rc: RabbitControlActor, val ex: ExecutionContext, val formats: Formats) extends RabbitConsumerIntegration