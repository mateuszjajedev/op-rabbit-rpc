package io.morgaroth.oprabbit

import com.typesafe.config.ConfigFactory
import org.scalatest.{FlatSpec, Matchers}

class ConfigurationTest extends FlatSpec with Matchers {

  val cfg = ConfigFactory.load()
  behavior of "Config"

  it should "load environment variables correctly" in {
    cfg.getString("amqp-server.vhost") shouldBe Option(System.getenv("AMQP_VHOST")).getOrElse("reference_vhost")
  }
}
